package eu.ensg.TP.metier;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Participant {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int num_pers;

	private String nom;

	private String prenom;
	
	private String email;
	
	private Date date_naiss;
	
	private String organisation;
	
	private String observations;

	
	public Participant(){
		   super();
	}
	
	public Participant(String nom, String prenom, String email, Date date_naiss, String organisation,
			String observations) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.date_naiss = date_naiss;
		this.organisation = organisation;
		this.observations = observations;
	}

	//getters and setters
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDate_naiss() {
		return date_naiss;
	}

	public void setDate_naiss(Date date_naiss) {
		this.date_naiss = date_naiss;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	
	
	
	
}