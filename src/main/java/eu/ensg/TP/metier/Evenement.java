package eu.ensg.TP.metier;



import java.util.Date;

import javax.persistence.*;

@Entity
public class Evenement {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int num_even;

	private String intitule;

	private String theme;
	
	private Date date_debut;
	
	private String description;
	
	
	public Evenement() {
		super();
	}
	
	
	public Evenement(String intitule, String theme, Date date_debut, String description) {
		this.intitule = intitule;
		this.theme = theme;
		this.date_debut = date_debut;
		this.description = description;
	}

	//getters and setters

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
}
