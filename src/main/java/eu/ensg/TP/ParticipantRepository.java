package eu.ensg.TP;

import org.springframework.data.repository.CrudRepository;

import eu.ensg.TP.metier.Participant;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ParticipantRepository extends CrudRepository<Participant, Integer> {

}